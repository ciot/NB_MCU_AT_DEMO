/** 
 *@file	     mcuInit.c
 *@brief	   硬件层功能
 *@copyright Copyright (c) 2023,利尔达物联网技术有限公司
 *           All rights reserved.
 *@author	   Lierda-技术应用组
 *@date      2023-7-20
 *@version   V1.0
*/

#include "AEP_Project.h"
#include "mcuInit.h"
#include "usart.h"
#include "main.h"
#include "adc.h"
#include "rtc.h"


/**
* @brief 定义RTC计数参数配置
*/
McuParaInit_s RtcPara =
{
		.HeartBeatCount       = 0,
		.PowerCheckCount      = 0,

};

/**    GPIO 初始化函数
 @brief     用户按照MCU平台，在此实现1302相关GPIO初始化
					  1、KEY_Pin:       按键接口
						2、LED_Pin:       LED接口
						3、GPIO_PIN_6:    模组 WEAKUP 接口
					 
 @return    函数执行结果： 
						- 成功：          true
						- 失败：          false
*/
bool GpioInit( void )
{
		GPIO_InitTypeDef GPIO_InitStruct = {0};

		/* GPIO Ports Clock Enable */
		__HAL_RCC_GPIOC_CLK_ENABLE();
		__HAL_RCC_GPIOA_CLK_ENABLE();

	  /*Configure GPIO pin Output Level */
		HAL_GPIO_WritePin(LED_GPIO_Port, LED_Pin, GPIO_PIN_RESET);
		HAL_GPIO_WritePin(WEAKUP_GPIO_Port, WEAKUP_Pin, GPIO_PIN_SET);
		
		
		/*Configure GPIO pin : PtPin */
		GPIO_InitStruct.Pin = KEY_Pin;
		GPIO_InitStruct.Mode = GPIO_MODE_IT_FALLING;
		GPIO_InitStruct.Pull = GPIO_NOPULL;
		HAL_GPIO_Init(KEY_GPIO_Port, &GPIO_InitStruct);
		
		/*Configure GPIO pin : PtPin */
		GPIO_InitStruct.Pin = LED_Pin;
		GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
		GPIO_InitStruct.Pull = GPIO_NOPULL;
		GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
		HAL_GPIO_Init(LED_GPIO_Port, &GPIO_InitStruct);
		
		
		/*Configure GPIO pin : PtPin */
		GPIO_InitStruct.Pin = WEAKUP_Pin;				//连接模组weakup引脚
		GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
		GPIO_InitStruct.Pull = GPIO_NOPULL;
		GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
		HAL_GPIO_Init(LED_GPIO_Port, &GPIO_InitStruct);
		
		HAL_NVIC_SetPriority(EXTI4_15_IRQn, 1, 0);
		HAL_NVIC_EnableIRQ(EXTI4_15_IRQn);
		
		return true;
}

/**    RTC 初始化函数
 @brief     用户按照MCU平台，在此实现 RTC 初始化
 @return    函数执行结果 
					  - 成功：          true
					  - 失败：          false
*/
bool RtcInit( void )
{
		/** Initialize RTC Only */
		hrtc.Instance = RTC;
		hrtc.Init.HourFormat = RTC_HOURFORMAT_24;
		hrtc.Init.AsynchPrediv = 127;
		hrtc.Init.SynchPrediv = 255;
		hrtc.Init.OutPut = RTC_OUTPUT_DISABLE;
		hrtc.Init.OutPutRemap = RTC_OUTPUT_REMAP_NONE;
		hrtc.Init.OutPutPolarity = RTC_OUTPUT_POLARITY_HIGH;
		hrtc.Init.OutPutType = RTC_OUTPUT_TYPE_OPENDRAIN;
		hrtc.Init.OutPutPullUp = RTC_OUTPUT_PULLUP_NONE;
		if (HAL_RTC_Init(&hrtc) != HAL_OK)
		{
				return false;
		}

		/** Enable the WakeUp */
		if (HAL_RTCEx_SetWakeUpTimer_IT(&hrtc, 2000, RTC_WAKEUPCLOCK_RTCCLK_DIV16) != HAL_OK)
		{
				return false;
		}
		return true;
}



/**    ADC 初始化函数
 @brief     用户按照MCU平台，在此实现 adc 初始化
 @return    函数执行结果 
					  - 成功：          true
					  - 失败：          false
*/
bool AdcInit( void )
{
		ADC_ChannelConfTypeDef sConfig = {0};

		/** Configure the global features of the ADC (Clock, Resolution, Data Alignment and number of conversion)*/
		hadc1.Instance = ADC1;
		hadc1.Init.ClockPrescaler = ADC_CLOCK_SYNC_PCLK_DIV2;
		hadc1.Init.Resolution = ADC_RESOLUTION_12B;
		hadc1.Init.DataAlign = ADC_DATAALIGN_RIGHT;
		hadc1.Init.ScanConvMode = ADC_SCAN_DISABLE;
		hadc1.Init.EOCSelection = ADC_EOC_SINGLE_CONV;
		hadc1.Init.LowPowerAutoWait = DISABLE;
		hadc1.Init.LowPowerAutoPowerOff = DISABLE;
		hadc1.Init.ContinuousConvMode = DISABLE;
		hadc1.Init.NbrOfConversion = 1;
		hadc1.Init.DiscontinuousConvMode = DISABLE;
		hadc1.Init.ExternalTrigConv = ADC_SOFTWARE_START;
		hadc1.Init.ExternalTrigConvEdge = ADC_EXTERNALTRIGCONVEDGE_NONE;
		hadc1.Init.DMAContinuousRequests = DISABLE;
		hadc1.Init.Overrun = ADC_OVR_DATA_PRESERVED;
		hadc1.Init.SamplingTimeCommon1 = ADC_SAMPLETIME_1CYCLE_5;
		hadc1.Init.SamplingTimeCommon2 = ADC_SAMPLETIME_1CYCLE_5;
		hadc1.Init.OversamplingMode = DISABLE;
		hadc1.Init.TriggerFrequencyMode = ADC_TRIGGER_FREQ_HIGH;
		if (HAL_ADC_Init(&hadc1) != HAL_OK)
		{
				return false;
		}
		/** Configure Regular Channel*/
		sConfig.Channel = ADC_CHANNEL_0;
		sConfig.Rank = ADC_REGULAR_RANK_1;
		sConfig.SamplingTime = ADC_SAMPLINGTIME_COMMON_1;
		if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
		{
				return false;
		}
		return true;
}


/**    UART 初始化函数
 @brief     用户按照MCU平台，在此实现 uart 初始化
            1、uart1与模组交互，波特率9600
						2、uart2与上位机交互，波特率115200
 @param[in] uart:             串口号
 @return    函数执行结果 
					  - 成功：          true
					  - 失败：          false
*/
bool UartInit( uint8_t uartx )
{
		if( uartx == 1 )
		{			
				huart1.Instance = USART1;
				huart1.Init.BaudRate = 9600;
				huart1.Init.WordLength = UART_WORDLENGTH_8B;
				huart1.Init.StopBits = UART_STOPBITS_1;
				huart1.Init.Parity = UART_PARITY_NONE;
				huart1.Init.Mode = UART_MODE_TX_RX;
				huart1.Init.HwFlowCtl = UART_HWCONTROL_NONE;
				huart1.Init.OverSampling = UART_OVERSAMPLING_16;
				huart1.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
				huart1.Init.ClockPrescaler = UART_PRESCALER_DIV1;
				huart1.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
				if (HAL_UART_Init(&huart1) != HAL_OK)
				{
					Error_Handler();
				}
				if (HAL_UARTEx_SetTxFifoThreshold(&huart1, UART_TXFIFO_THRESHOLD_1_8) != HAL_OK)
				{
					Error_Handler();
				}
				if (HAL_UARTEx_SetRxFifoThreshold(&huart1, UART_RXFIFO_THRESHOLD_1_8) != HAL_OK)
				{
					Error_Handler();
				}
				if (HAL_UARTEx_DisableFifoMode(&huart1) != HAL_OK)
				{
					Error_Handler();
				}
		}

		if( uartx == 2 )
		{
				huart2.Instance = USART2;
				huart2.Init.BaudRate = 115200;
				huart2.Init.WordLength = UART_WORDLENGTH_8B;
				huart2.Init.StopBits = UART_STOPBITS_1;
				huart2.Init.Parity = UART_PARITY_NONE;
				huart2.Init.Mode = UART_MODE_TX_RX;
				huart2.Init.HwFlowCtl = UART_HWCONTROL_NONE;
				huart2.Init.OverSampling = UART_OVERSAMPLING_16;
				huart2.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
				huart2.Init.ClockPrescaler = UART_PRESCALER_DIV1;
				huart2.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
				if (HAL_UART_Init(&huart2) != HAL_OK)
				{
					Error_Handler();
				}
				if (HAL_UARTEx_SetTxFifoThreshold(&huart2, UART_TXFIFO_THRESHOLD_1_8) != HAL_OK)
				{
					Error_Handler();
				}
				if (HAL_UARTEx_SetRxFifoThreshold(&huart2, UART_RXFIFO_THRESHOLD_1_8) != HAL_OK)
				{
					Error_Handler();
				}
				if (HAL_UARTEx_DisableFifoMode(&huart2) != HAL_OK)
				{
					Error_Handler();
				}
		} 
		Uart1RegCb( Uart1RxCb );		
		return true;
}

/**    DMA 初始化函数
 @brief     用户按照MCU平台，在此实现 DMA 初始化
 @return    函数执行结果 
					  - 成功：          true
					  - 失败：          false
*/
bool DmaInit( void )
{

  /* DMA controller clock enable */
  __HAL_RCC_DMA1_CLK_ENABLE();

  /* DMA interrupt init */

  HAL_NVIC_SetPriority(DMA1_Channel1_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(DMA1_Channel1_IRQn);

  HAL_NVIC_SetPriority(DMA1_Channel2_3_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(DMA1_Channel2_3_IRQn);
	
	return true;
}


/**
 @brief   系统时钟配置

*/
void SysClockConfig(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Configure the main internal regulator output voltage
  */
  HAL_PWREx_ControlVoltageScaling(PWR_REGULATOR_VOLTAGE_SCALE1);

  /** Initializes the RCC Oscillators according to the specified parameters in the RCC_OscInitTypeDef structure. */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSIDiv = RCC_HSI_DIV1;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
  RCC_OscInitStruct.PLL.PLLM = RCC_PLLM_DIV1;
  RCC_OscInitStruct.PLL.PLLN = 8;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLQ = RCC_PLLQ_DIV2;
  RCC_OscInitStruct.PLL.PLLR = RCC_PLLR_DIV2;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
	
	RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_LSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSIDiv = RCC_HSI_DIV1;
  RCC_OscInitStruct.HSICalibrationValue = 16;
  RCC_OscInitStruct.LSIState = RCC_LSI_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_NONE;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
	
  /** Initializes the CPU, AHB and APB buses clocks*/
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK|RCC_CLOCKTYPE_PCLK1;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
  {
    Error_Handler();
  }
}

/**    获取指定 ADC 通道的数据
 @brief       用户按照实际功能，调用此函数实现获取ADC电压值
 @return:     函数执行结果 
              - 获取的电压值：voltage
 */
float GetAdcData( void )
{
		uint16_t  adcValue    = 0;
		float     voltage     = 0.0;                // 电压值	

		HAL_ADC_Start( &hadc1 );	                  // 启动ADC转换
		HAL_ADC_PollForConversion( &hadc1, 100 );	  // 等待转换完成，第二个参数表示超时时间，单位ms
		adcValue = HAL_ADC_GetValue( &hadc1 );	    // 换取ADC状态
	
		voltage = ( float )adcValue / 4096 * 3.3;	  // 采取的右对齐除以2的12次方，参考电压为3.3V
	
    return voltage;
}


/**    获取系统时间
 @brief       获取当前系统的时间，单位为ms
 @return:     函数执行结果 
              - 系统时间：    time
 */
uint32_t GetSystemTime( void )
{
		uint32_t time;
	
		time = HAL_GetTick( );
		return time;
}


/**  	 获取接收缓存函数
 @brief       用户根据实际功能，调用此函数实现接收缓存获取
 @param[in]   uart：          串口号
 @param[in]   buff：    	    接收缓存值
 @return      函数执行结果 
							- 接收缓存：    RXBuff
*/
const uint8_t* GetRxBuff( uint8_t uart, const uint8_t* buff )
{
		UART_RX_s* uartrx;
		
		uartrx = getuart( uart );
		buff   = uartrx->RXBuff;
	
		return buff;
}
const uint8_t* GetRxBuf( uint8_t uart, const uint8_t* buff )//获取接收缓存函数，只用来处理平台下发的数据
{
		UART_RX_s* uartrx;
		
		uartrx = getuart( uart );
		buff   = uartrx->rxBuff;
	
		return buff;
}

/**  	 获取接收标志位函数
 @brief       用户根据实际功能，调用此函数实现标志位获取
 @param[in]   uart            串口号
 @return      函数执行结果 
							- 标志位值：    rxDoneFlg
*/
uint8_t GetRxFlag( uint8_t uart )
{
		UART_RX_s* uartrx;
		
		uartrx = getuart( uart );
	
		return uartrx->rxDoneFlg;
}


/**  	 设置接收标志位函数
 @brief       用户根据实际功能，调用此函数实现接收标志位置位
 @param[in]   uart：        串口号
 @param[in]   flg：    	    待配置的flag值
 @return      函数执行结果 
							- 失败：      FALSE
*/
uint8_t SetRxFlag( uint8_t uart, uint8_t flg )
{
		UART_RX_s* uartrx;
	
		uartrx = getuart( uart );
		uartrx->rxDoneFlg = flg;
	
		return  true;
}


/**  	 RTC 定时器回调函数
 @brief       此函数实现 MCU 1 秒唤醒后，进行系统时钟重新配置、打开Systick的中断等操作
*/
void HAL_RTCEx_WakeUpTimerEventCallback( RTC_HandleTypeDef *hrtc )
{
		__HAL_PWR_CLEAR_FLAG( PWR_FLAG_WUF );

		SysClockConfig( );                            // 配置系统时钟
		SysTick->CTRL  = SysTick_CTRL_CLKSOURCE_Msk | SysTick_CTRL_TICKINT_Msk | SysTick_CTRL_ENABLE_Msk; // 打开Systick的中断
		SCB->SCR &= ~SCB_SCR_SLEEPONEXIT_Msk;         // 退出中断时不再自动进入低功耗模式
		
		RtcPara.HeartBeatCount++;
		RtcPara.PowerCheckCount++;
}


int GetRtcTick( uint8_t type )
{
		if( type == 1 )
		{
				return RtcPara.HeartBeatCount;
		}
		if( type == 2 )
		{
				return RtcPara.PowerCheckCount;
		}
		return true;
}



int SetRtcTick( uint8_t type, uint8_t value )
{
		if( type == 1 )
		{
				RtcPara.HeartBeatCount = value;
				return value;
		}
		if( type == 2 )
		{
				RtcPara.PowerCheckCount = value;
				return value;
		}
		return true;
}


/**  	 进入SOP1模式
 @brief       用户根据实际功能，调用此函数使mcu进入低功耗STOP1模式
 @return      函数执行结果 
							- 成功：      TRUE
*/
uint8_t StartEnterStop1( void ) 
{
		__HAL_RCC_PWR_CLK_ENABLE( );                                              // 打开电源控制时钟
		SysTick->CTRL  = SysTick_CTRL_CLKSOURCE_Msk |0| SysTick_CTRL_ENABLE_Msk;  // 关闭系统Systick计时中断 
	
		HAL_PWR_EnterSTOPMode( PWR_LOWPOWERREGULATOR_ON, PWR_STOPENTRY_WFI );     // 进入STOP模式

		return  true;
}



/**  	 Mcu 参数初始化函数
 @brief       用户根据实际功能，在此函数实现 MCU 参数初始化赋值
*/
void RtcTickInit( void )
{
		RtcPara.HeartBeatCount  = 0;
		RtcPara.PowerCheckCount = 0;

}



