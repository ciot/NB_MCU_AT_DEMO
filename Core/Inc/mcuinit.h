/** 
 *@file	     mcuInit.h
 *@brief	   硬件层功能
 *@copyright Copyright (c) 2023,利尔达物联网技术有限公司
 *           All rights reserved.
 *@author	   Lierda-技术应用组
 *@date      2023-7-20
 *@version   V1.0
 */



#include <string.h>
#include <stdbool.h>
#include <stdint.h>


/* -------------------------------------------------------------------------- */


/**
* @enum   McuParaInit_s
* @brief  定义mcu参数配置类型
*/
typedef struct
{
	uint8_t  HeartBeatCount;            //< 心跳计数
	uint8_t  PowerCheckCount;           //< 电压检测计数

}McuParaInit_s;


/* ---硬件引脚定义 -------------------------------------------------- */

/** 设置连接MCU 按键引脚 GPIO口 */
#define  KEY_Pin                       GPIO_PIN_13
#define  KEY_GPIO_Port                 GPIOC

/** 设置连接MCU LED引脚 GPIO口 */
#define  LED_Pin                       GPIO_PIN_5
#define  LED_GPIO_Port                 GPIOA

/** 设置连接MCU WEAKUP引脚 GPIO口 */
#define  WEAKUP_Pin                    GPIO_PIN_6
#define  WEAKUP_GPIO_Port              GPIOA

/** 设置连接模块WEAKUP引脚的为低电平 */
#define  MB26_WEAKUP_LOW		  	       HAL_GPIO_WritePin( GPIOA, GPIO_PIN_6, GPIO_PIN_RESET );

/** 设置连接模块WEAKUP引脚的为高电平 */
#define  MB26_WEAKUP_HIGH		           HAL_GPIO_WritePin( GPIOA, GPIO_PIN_6, GPIO_PIN_SET );

/** 设置连接MCU LED引脚的为高电平 */
#define  LED_GPIO_ON		               HAL_GPIO_WritePin(GPIOA, GPIO_PIN_5, GPIO_PIN_SET);

/** 设置连接MCU LED引脚的为低电平 */
#define  LED_GPIO_OFF		               HAL_GPIO_WritePin(GPIOA, GPIO_PIN_5, GPIO_PIN_RESET);

/** 设置 LED引脚电平翻转 */
#define  LED_GPIO_ON_OFF		           HAL_GPIO_TogglePin( GPIOA, GPIO_PIN_5 );

/** 读取连接MCU 按键引脚电平 */
#define  GET_KEY_LEVEL                HAL_GPIO_ReadPin( GPIOC, GPIO_PIN_13 )


/* -------------------------------------------------------------------------- */

/*****************************************************************
* Function:     GpioInit
*
* Description:  用户按照 MCU 平台，在此实现 MCU 相关 GPIO 初始化 
*					      1、KEY_Pin:       按键接口
*						    2、LED_Pin:       LED接口
*						    3、GPIO_PIN_6:    模组复位接口
* Return:
* 	true			  初始化成功。
*		false 	    初始化失败。
*
*****************************************************************/
bool           GpioInit( void );


/*****************************************************************
* Function:     AdcInit
*
* Description:  用户按照 MCU 平台，在此实现 MCU 相关 adc 初始化 
*
* Return:
* 	true			  初始化成功。
*		false 	    初始化失败。
*
*****************************************************************/
bool           AdcInit( void );


/*****************************************************************
* Function:     RtcInit
*
* Description:  用户按照 MCU 平台，在此实现 MCU 相关 RTC 初始化，MCU 无任务时会进入休眠状态，RTC 定时器用于1S唤醒一次 MCU 
*
* Return:
* 	true			  初始化成功。
*		false 	    初始化失败。
*
*****************************************************************/
bool           RtcInit( void );


/*****************************************************************
* Function:     UartInit
*
* Description:  用户按照 MCU 平台，在此实现 MCU 相关 uart 初始化。
*
* Parameters:
*   uartx				[in] 	串口号：
*                      1 表示串口1，MCU 与模组交互，波特率9600。
*                      2 表示串口2，MCU 与上位机交互，波特率115200。
* Return:
* 	true			  初始化成功。
*		false 	    初始化失败。
*
* Warning：     传入的参数不可为空
*
*****************************************************************/
bool           UartInit( uint8_t uartx );


/*****************************************************************
* Function:     DmaInit
*
* Description:  用户按照 MCU 平台，在此实现 MCU 相关 DMA 初始化。
*
* Return:
* 	true			  初始化成功。
*		false 	    初始化失败。
*
*****************************************************************/
bool           DmaInit( void );


/*****************************************************************
* Function:     GetAdcData
*
* Description:  用户按照 MCU 平台，在此实现获取指定 ADC 通道电压值。
*
* Return:
* 	voltage			获取的电压值。
*
*****************************************************************/
float          GetAdcData( void );


/*****************************************************************
* Function:     StartEnterStop1
*
* Description:  
*		此函数实现 MCU 进入低功耗前的准备，并进入低功耗 STOP1 模式。
*
*****************************************************************/
uint8_t        StartEnterStop1( void );


/*****************************************************************
* Function:     SysClockConfig
*
* Description:  
*		用户按照 MCU 平台，在此实现系统时钟配置。
*
*****************************************************************/
void           SysClockConfig(void);


/*****************************************************************
* Function:     GetSystemTime
*
* Description:  
*		该函数实现获取当前系统的时间功能，单位为ms。
*
*****************************************************************/
uint32_t       GetSystemTime( void );


/*****************************************************************
* Function:     GetRxBuff
*
* Description:  
*		该函数实现串口接收缓存地址获取。
*
* Parameters:
*   uart				[in] 	串口号：1 表示uart1，2 表示uart2.
*   buff				[in] 	接收缓存指针
*
* Return:
* 	RXBuff			接收缓存地址。
*
* Warning：     传入的参数及参数指针不可为空
*
*****************************************************************/
const uint8_t* GetRxBuff( uint8_t uart, const uint8_t* buff );


/*****************************************************************
* Function:     GetRxFlag
*
* Description:  
*		该函数实现串口接收标志位地址获取。
*
* Parameters:
*   uart				[in] 	串口号：1 表示uart1，2 表示uart2.
*
* Return:
* 	rxDoneFlg		串口接收标志位地址。
*
* Warning：     传入的参数不可为空
*
*****************************************************************/
uint8_t        GetRxFlag( uint8_t uart );


/*****************************************************************
* Function:     SetRxFlag
*
* Description:  
*		该函数实现串口接收标志位设置。
*
* Parameters:
*   uart				[in] 	串口号：1 表示uart1，2 表示uart2。
*   Flg 				[in] 	标志位值：true\false
*
* Return:
* 	true    		标志位置位成功。
*
* Warning：     传入的参数不可为空
*
*****************************************************************/
uint8_t        SetRxFlag( uint8_t uart, uint8_t Flg );


/*****************************************************************
* Function:     RtcTickInit
*
* Description:  
*		用户根据实际功能，在此函数实现 RTC 计数初始化。
*
*****************************************************************/
void           RtcTickInit( void );


/*****************************************************************
* Function:     GetRtcTick
*
* Description:  
*		该函数实现 RTC 定时器计数值获取。
*
* Parameters:
*   type				[in] 	计数类型：1 表示心跳计数，2 表示电压检测计数。
*
* Return:
* 	心跳计数值        RtcPara.HeartBeatCount
*   电压检测计数值    RtcPara.PowerCheckCount
*
* Warning：     传入的参数不可为空
*
*****************************************************************/
int           GetRtcTick(  uint8_t type );


/*****************************************************************
* Function:     SetRtcTick
*
* Description:  
*		该函数实现 RTC 定时器计数值配置。
*
* Parameters:
*   type				[in] 	计数类型：1 表示心跳计数，2 表示电压检测计数。
*   value       [in] 	计数值，整型。
*
* Return:
* 	value       配置的计数值
*
* Warning：     传入的参数不可为空
*
*****************************************************************/
int           SetRtcTick( uint8_t type, uint8_t value );



const uint8_t* GetRxBuf( uint8_t uart, const uint8_t* buff );
