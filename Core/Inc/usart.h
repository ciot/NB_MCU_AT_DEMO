/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file    usart.h
  * @brief   This file contains all the function prototypes for
  *          the usart.c file
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2023 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __USART_H__
#define __USART_H__

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* USER CODE BEGIN Includes */
#define UART_MAXLEN_RX 512
#define USART_USER_BUFF_LEN 512

typedef struct
{
    uint8_t rxDoneFlg;
    uint8_t rxLen;
    uint8_t rxBuff[ UART_MAXLEN_RX ];
		uint8_t	RXBuff[ UART_MAXLEN_RX ];
} UART_RX_s;

/* USER CODE END Includes */

UART_RX_s* getuart(uint8_t uart);

extern UART_HandleTypeDef huart1;

extern UART_HandleTypeDef huart2;

/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

void MX_USART1_UART_Init(void);
void MX_USART2_UART_Init(void);
void Uart2Receive_IDLE( UART_HandleTypeDef* huart );
void Uart1Receive_IDLE( UART_HandleTypeDef* huart );

/*****************************************************************
* Function:     RecCallback
*
* Parameters:
*   recBuf		  [in] 	 串口接收缓存
*   len 		    [in] 	 接收缓存长度
*
*****************************************************************/
typedef int    ( *RecCallback )( uint8_t* recBuf, uint32_t len );



int  Uart1RegCb( RecCallback pfunc );

/* USER CODE BEGIN Prototypes */

/* USER CODE END Prototypes */

#ifdef __cplusplus
}
#endif

#endif /* __USART_H__ */

