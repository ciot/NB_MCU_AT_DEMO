/*!
 *@file	     AEP_Project.c
 *@brief	   应用层功能
 *@copyright Copyright (c) 2023,利尔达物联网技术有限公司
 *           All rights reserved.
 *@author	   Lierda-技术应用组
 *@date      2023-7-20
 *@version   V1.0
 */

#include "AEP_Project.h"
#include "MB26-S_Drive.h"
#include "NB-IOT_Ports.h"
#include "mcuInit.h"
#include "log.h"
                           
#define   SEVER_AEP_URL	          "221.229.214.202,5683,86400"	        //	连接配置参数：severID、端口号、保活时间
#define   USER_AEP_DATA           "6c696572646131323334353637383930"    //  用户发送数据：自定义
#define   CTIVE_TIME              "00100100"                            //   模组PSM功能启用时配置激活时间值

#define   SEND_DATA_TIME_S        15                 //定时发送心跳包时间，15秒
#define   VOL_CHECK_TIME_S        50                 //定时电压检测时间，20秒

#define   NB_INIT_TIME_180S       1000*60*3          //NB初始化时间, 3分钟
#define   UNUS_HANDLE_60S         1000*60          	 //异常流程处理时间, 1分钟
#define   NET_CHECK_90S           90                 //检查是否入网时间, 90 秒

#define   KEY_DEBOUNCE_160MS      8                  //按键消抖连续检测次次数，8次

#define   RESEND_DATA_NUM         3                  //用户数据上报重传次数，3次
#define   RESEND_AT_ERROR         3                  //AT指令回复失败重发次数，3次

#define   ENABLE_BAN_PSM          1                  //模组PSM功能禁用或启用，0表示禁用PSM、1表示启用PSM	
#define   ENABLE_BAN_PMU          1                  //开启或禁止PMU功能，0表示禁用PMU，系统直接进入IDLE态、1表示启用PMU，此时可以配置模组不同睡眠等级

#define   SLEEP_STATUS_ACTIVE     0                  //模组睡眠深度级别，ACTIVE态
#define   SLEEP_STATUS_HIBERNATE  4                  //模组睡眠深度级别，HIBERNATE态


/** 模组初始化任务 */
InitEvent_t      g_InitTask;

/** 异常处理任务 */
UnuaualEvent_t   g_ErrorTask;

TaskStatus_s status=
{
		.g_moubleInitFlag       = 0,  	  /*!< 模组初始化标志位 */
		.g_sleepModeFlag        = 0,  	  /*!< 模组休眠模式标志位 */
		.g_sendingFlag          = 0,     	/*!< 数据发送标志位 */
		.g_keyAlarmFlag         = 0,     	/*!< 按键按下标志位 */
		.g_powerLowFlag         = 0,  	  /*!< 模组电压检测标志位 */
};


/**    模组初始化函数
 @brief     该函数按照一定逻辑，实现MB26-S模组初始化流程

*/
void NbIotInit( void )
{
		MB26_WEAKUP_LOW;                      //上电weakup脚拉低,唤醒模组
		SysDelayMs( 500 );                    //等待模组唤醒成功
	
		uwTick = 0;                           //滴答定时器计数置0

		while( ( status.g_moubleInitFlag != 1 )&&( uwTick < NB_INIT_TIME_180S ) )  //180秒内循环NB-IOT初始化
		{
				if( INIT_SUCCEED == AepConnect( ) )
					break;
		}
		if( ( status.g_moubleInitFlag != 1 )&&( uwTick > NB_INIT_TIME_180S ) )     //如果时间超出，即还未初始化成功
		{
				Log_Printf( "\r\n= 模组初始化失败 !!! =\r\n" );
				MB26_WEAKUP_HIGH;                                       //模组进休眠模式
				SysDelayMs( 30 );
				LedBlink( );
		}
		else
		{
				Log_Printf( "\r\n= 模组初始化完成 !!! =\r\n" );
				MB26_WEAKUP_HIGH;                                       //模组进休眠模式
				SysDelayMs( 30 );  
		}
		uwTick = 0; 
}


/**    AEP 平台连接函数
 @brief     用户按照MCU平台，在此实现MB26-S模组与AEP平台连接流程
 @return    函数执行结果： 
						- 成功：          INIT_SUCCEED
						- 失败：          INIT_FAILED
*/
uint8_t AepConnect( void )
{
		static int times = 0;
	
		switch(	g_InitTask )
		{
			case AtCheck:
					status.g_moubleInitFlag = 0;
			
					if( AT_SUCCESS != AtChecke( ) )
					{
							if( ++times >= RESEND_AT_ERROR )
							{
									times = 0;
									return INIT_FAILED;
							}
							break;
					}
					g_InitTask = simStatus;
					break;
				
			case simStatus:
					if( AT_SUCCESS != GetSimStatus( ) )
					{
							if( ++times >= RESEND_AT_ERROR )
							{
									g_InitTask = moduleReset;
									times = 0;
							}
							break;
					}
					else
					{
						g_InitTask = psStatus;
					}
					break;	
					
			case psStatus:
					if( DEVICE_ONLINE != GetNetStatus( ) )
					{
							if( ++times >= NET_CHECK_90S )
							{
									times = 0;
									g_InitTask = AtCheck;
									return INIT_FAILED;
							}
							break;
					}
					g_InitTask = deviceImei;
					break;
					
			case moduleReset:
					if( AT_SUCCESS != ModuleSoftReset( ) )
					{
							if( ++times >= RESEND_AT_ERROR )
							{
									times = 0;
									g_InitTask = AtCheck;
									return INIT_FAILED;
							}
							break;
					}
					g_InitTask = AtCheck;
					break;
						
			case deviceImei:
					if( AT_SUCCESS != GetDeviceImei( ) )
					{
							if( ++times >= RESEND_AT_ERROR )
							{
									times = 0;
									g_InitTask = AtCheck;
									return INIT_FAILED;
							}
							break;
					}
					g_InitTask = banSleep;
					break;	
					
			case banSleep:
					if( AT_SUCCESS != SetSleepMode( ENABLE_BAN_PMU, SLEEP_STATUS_ACTIVE ) )
					{
							if( ++times >= RESEND_AT_ERROR )
							{
									times = 0;
									g_InitTask = AtCheck;
									return INIT_FAILED;
							}
							break;
					}
					g_InitTask = connePara;
					break;	
					
			case connePara:
					if( AT_SUCCESS != SetConnePara( SEVER_AEP_URL ) )
					{
							if( ++times >= RESEND_AT_ERROR )
							{
									times = 0;
									g_InitTask = AtCheck;
									return INIT_FAILED;
							}
							break;
					}
					g_InitTask = regRequest;
					break;	
					
			case regRequest:
					if( AT_SUCCESS != SendRegReq( ) )
					{
							if( ++times >= RESEND_AT_ERROR )
							{
									g_InitTask = cancelRequest;
									times = 0;
							}
							break;
					}
					else
					{
							g_InitTask = closeEdrx;
					}
					break;	
					
			case cancelRequest:
					if( AT_SUCCESS != SendCancelReq( ) )
					{
							if( ++times >= RESEND_AT_ERROR )
							{
									times = 0;
									g_InitTask = AtCheck;
									return INIT_FAILED;
							}
							break;
					}
					g_InitTask = regRequest;
					break;	
					
			case closeEdrx:
					if( AT_SUCCESS != CloseModuleEdrx( ) )
					{
							if( ++times >= RESEND_AT_ERROR )
							{
									times = 0;
									g_InitTask = AtCheck;
									return INIT_FAILED;
							}
							break;
					}
					g_InitTask = setPsm;
					break;	
					
			case setPsm:
					if( AT_SUCCESS != SetModulePsm( ENABLE_BAN_PSM, CTIVE_TIME ) )
					{
							if( ++times >= RESEND_AT_ERROR )
							{
									times = 0;
									g_InitTask = AtCheck;
									return INIT_FAILED;
							}
							break;
					}
					g_InitTask = enableSleep;
					break;
					
			case enableSleep:
					if( AT_SUCCESS != SetSleepMode( ENABLE_BAN_PMU, SLEEP_STATUS_HIBERNATE ) )
					{
							if( ++times >= RESEND_AT_ERROR )
							{
									times = 0;
									g_InitTask = AtCheck;
									return INIT_FAILED;
							}
							break;
					}
					status.g_moubleInitFlag = 1;
					return INIT_SUCCEED;
					
			default:      
					break;
		}
}	


/**    心跳发送函数
 @brief     此函数实现定时数据上报功能
						定时时间到，先唤醒模组，再发送数据，数据上报成功后使模组休眠；若数据上报失败，则执行异常处理流程
*/
void HeartBeatSend(void)
{
	  int rev;   

		if( GetRtcTick( 1 ) >= SEND_DATA_TIME_S )		          //定时时间到
		{
				Log_Printf( "\r\n= 定时时间到: =\r\n" );
				status.g_sendingFlag = 1;
			
				MB26_WEAKUP_LOW;                              //拉低weakup引脚，唤醒模组
				SysDelayMs( 30 );
				SetSleepMode( ENABLE_BAN_PMU, SLEEP_STATUS_ACTIVE );
				
				for( int i = 0; i < RESEND_DATA_NUM; i++ )		//在重发次数内循环发送，若发送成功，则跳出循环，结束发送
				{
						if( AT_SUCCESS == SendUserData( USER_AEP_DATA ) )
						{
								rev = AT_SUCCESS;
								break;
						}
				}
				if( rev != AT_SUCCESS )                    		//循环发送一定次数后仍失败，则进异常处理
				{
						uwTick = 0;                               //滴答定时器计数置0
						while( uwTick < UNUS_HANDLE_60S )   			//一定时间内执行异常处理
						{
								if( HANDLE_SUCCEED == UnusualHandle( ) )
								{
										rev = AT_SUCCESS;
										break;
								}
						}
						if( rev == AT_SUCCESS )
						{
								if( AT_SUCCESS != SendUserData( USER_AEP_DATA ) )   //上报数据
										Log_Printf( "\r\nSend data failed !!!\r\n" );
						}
				}

				ReleasRrc( );                            			//释放RRC

				MB26_WEAKUP_HIGH;                        			//模组进休眠模式
				SysDelayMs( 50 );
				status.g_sendingFlag = 0;
				SetRtcTick( 1, 0 );
		}
}

/**    按键报警函数
 @brief     此函数实现按键报警功能
						检测到按键按下，先唤醒模组，再发送数据，数据上报成功后使模组休眠；若数据上报失败，则执行异常处理流程
*/
void KeyAlarm( void )
{
		int rev = -1;
	
		for( int cnt = 0; cnt < KEY_DEBOUNCE_160MS; cnt++ )       //按键消抖
		{
				if( GET_KEY_LEVEL )                                   //读取按键引脚电平，检测按键是否被按下
					return;
				SysDelayMs( 20 );                                     //检测周期20ms
		}

		Log_Printf( "\r\n= 按键按下 =\r\n" );
		status.g_keyAlarmFlag = 1;
		
		MB26_WEAKUP_LOW;                                         //唤醒模组
		SysDelayMs( 30 );
		
		LED_GPIO_ON_OFF;                                         //LED常亮报警
 
		for( int i = 0; i < RESEND_DATA_NUM; i++ )		//在重发次数内循环发送，若发送成功，则跳出循环，结束发送
		{
				if( AT_SUCCESS == SendUserData( USER_AEP_DATA ) )
				{
						rev = AT_SUCCESS;
						break;
				}
		}
		if( rev != AT_SUCCESS )                    		//循环发送一定次数后仍失败，则进异常处理
		{
				uwTick = 0;                               //滴答定时器计数置0
				while( uwTick < UNUS_HANDLE_60S )  
				{
						if( HANDLE_SUCCEED == UnusualHandle( ) )
						{
								rev = AT_SUCCESS;
								break;
						}
				}
				if( rev == AT_SUCCESS )
				{
						if( AT_SUCCESS != SendUserData( USER_AEP_DATA ) )   //上报数据
								Log_Printf( "\r\nSend data failed !!!\r\n" );
				}
		}

		LED_GPIO_ON_OFF;                             //LED等灭
		
		ReleasRrc( );                                //释放RRC
		
		MB26_WEAKUP_HIGH;                            //模组进休眠模式
		SysDelayMs( 50 );
		status.g_keyAlarmFlag = 0;
}


/**    电压检测函数
 @brief     此函数实现电压检测功能
						检测电压异常，使能模组休眠，LED灯常量报警；电压正常，关闭LED灯
*/
void AdcCheck( void )
{
		float  voltage  = 0.0; // 电压值	
	
		if( GetRtcTick( 2 ) >= VOL_CHECK_TIME_S )
		{
				status.g_powerLowFlag = 1;
			
				voltage = GetAdcData( );     //获取 ADC 通道电压值
			
				if( voltage >= 2.4 )
				{
						Log_Printf( "\r\n= 电压正常 : =\r\n" );
						LED_GPIO_OFF;
				}
				else
				{
						Log_Printf( "\r\n= 电压异常 : =\r\n" );
						LED_GPIO_ON;                            //LED灯常亮
					
						MB26_WEAKUP_HIGH;                       //模组进休眠模式
						SysDelayMs( 50 );                    //拉高weakuo引脚电平，模组休眠
				}
				Log_Printf( "\r\nMouble voltage ADC : %.3f V\r\n", voltage );
				status.g_powerLowFlag = 0;
				SetRtcTick( 2, 0 );
		}
}


/*!
 *@brief 异常处理流程
 */
uint8_t UnusualHandle( void )
{
		static int times = 0;
	
		switch(	g_ErrorTask )
		{
			case RadioOff:
					if( AT_SUCCESS != FuncRadio( 0 ) )
					{
							if( ++times >= 3 )
							{
									times = 0;
									return HANDLE_FAILED;
							}
							break;
					}
					g_ErrorTask = FreqClear;
					break;
		
			case FreqClear:
					if( AT_SUCCESS != ClearFreq( ) )
					{
							if( ++times >= 3 )
							{
									times = 0;
									return HANDLE_FAILED;
							}
							break;
					}
					g_ErrorTask = RadioOn;
					break;
					
			case RadioOn:
					if( AT_SUCCESS != FuncRadio( 1 ) )
					{
							if( ++times >= 3 )
							{
									times = 0;
									g_ErrorTask = RadioOff;
									return HANDLE_FAILED;
							}
							break;
					}else
					{
							return HANDLE_SUCCEED;
					}
					
			default:      
					break;
		}
}	


/**    LED闪烁
 @brief     此函数实现2秒内，LED闪烁功能
*/
void LedBlink( void )
{
		uint32_t  recordTime    = 0;
	
		while( ( GetSystemTime( ) - recordTime ) < 2000 )   //超时时间内，接收模组返回的数据
		{
				LED_GPIO_ON; 
				SysDelayMs( 500 );
				LED_GPIO_OFF;
		}			
}	


/**    串口接收回调函数
 @brief       此函数实现对平台下发的数据进行判断、处理
 */
int Uart1RxCb( uint8_t* recBuf, uint32_t len )
{
		char  value;
		char  *offset = NULL;
	  const uint8_t*  buff;
	
		if( strstr( ( char* )GetRxBuf( 1, buff ), "+CTM2MRECV:" ) != NULL )	//	V2.0改了接收回调处理，平台主动下发的数据缓存和模组回应的缓存分开处理
		{	
				offset = strstr( ( char* )GetRxBuf( 1, buff ), ":" );
				sscanf( &offset[ 2 ], "%s", &value );
			
				memset( ( char* )GetRxBuf( 1, buff ), 0, 512 );   //清空接收缓存
			
				Log_Printf( "Sevice send data: %s\r\n", &value );
		}
		return true;
}

/**    
 @brief      MCU进入低功耗模式
*/
void EnterStop1Mode( void )  
{
		if(	( status.g_sendingFlag == 1 )||(	status.g_keyAlarmFlag == 1 )||( status.g_powerLowFlag == 1 ) )
				return;
		
		StartEnterStop1( );

}






