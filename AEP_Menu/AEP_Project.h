/*!
 *@file	     AEP_Project.h
 *@brief	   应用层功能
 *@copyright Copyright (c) 2023,利尔达物联网技术有限公司
 *           All rights reserved.
 *@author	   Lierda-技术应用组
 *@date      2023-7-20
 *@version   V1.0
 */


#include <stdbool.h>
#include <stdint.h>
#include <stddef.h>


typedef struct
{
		bool g_moubleInitFlag;       /*!< 模组初始化标志位 */
		bool g_sleepModeFlag;        /*!< 模组休眠模式标志位 */
	  bool g_sendingFlag;          /*!< 数据发送标志位 */
    bool g_keyAlarmFlag;         /*!< 按键按下标志位 */
    bool g_powerLowFlag;         /*!< 模组电压检测标志位 */
}TaskStatus_s;


typedef enum
{
		AtCheck       = 0,           /*!< AT上电检测 */
		simStatus     = 1,           /*!< 查询SIM卡状态 */  
		psStatus      = 2,           /*!< 查询网络状态 */ 
		moduleReset   = 3,           /*!< 模组软复位 */ 
		deviceImei    = 4,           /*!< 查询设备IMEI号 */ 
		banSleep      = 5,           /*!< 禁止模组休眠 */ 
		connePara     = 6,           /*!< 网络连接参数 */ 
		regRequest    = 7,           /*!< 注册平台请求 */
		cancelRequest = 8,           /*!< 注销平台请求 */
		closeEdrx     = 9,           /*!< 关闭Edrx */
		setPsm        = 10,          /*!< 配置PSM功能 */
		enableSleep   = 11           /*!< 使能模组休眠 */
} InitEvent_t;



typedef enum
{
		RadioOff      = 0,           /*!< 关闭射频 */
		FreqClear     = 1,           /*!< 清除记忆频点 */ 
		RadioOn       = 2            /*!< 开启射频 */
} UnuaualEvent_t;


/*****************************************************************
* Function:     NbIotInit
*
* Description:  
*		该函数按照一定逻辑，实现 NB-IOT 初始化流程。
*
*****************************************************************/
void     NbIotInit( void );


/*****************************************************************
* Function:     AepConnect
*
* Description:  
*		该函数按照一定逻辑，在此实现MB26-S模组 AEP 平台连接流程。
*
* Return:
* 	INIT_SUCCEED	     初始化成功。
*		INIT_FAILED 	     初始化失败。
*
*****************************************************************/
uint8_t   AepConnect( void );


/*****************************************************************
* Function:     KeyAlarm
*
* Description:  
*		此函数实现按键报警功能，若检测到按键按下，先唤醒模组，再上报数据，上报数据完成后模组休眠；若数据上报失败，则执行异常处理流程。
*
*****************************************************************/
void      KeyAlarm( void );


/*****************************************************************
* Function:     AdcCheck
*
* Description:  
*		此函数实现每隔2h,进行一次电压检测功能，若检测电压异常（小于2.4V），使能模组休眠，LED灯常量报警；若检测到电压正常（大于2.4V），关闭LED灯。
*
*****************************************************************/
void      AdcCheck( void );


/*****************************************************************
* Function:     HeartBeatSend
*
* Description:  
*		此函数实现每隔15分钟，进行一次数据上报功能，若定时时间到，先唤醒模组，再发送数据，若数据发送成功，模组休眠；若发送失败，则执行异常处理流程。
*
*****************************************************************/
void      HeartBeatSend(void);


/*****************************************************************
* Function:     UnusualHandle
*
* Description:  
*		该函数按照一定逻辑，在此实现模组异常处理流程。
*
* Return:
* 	HANDLE_SUCCEED	   处理成功。
*		HANDLE_FAILED 	   处理失败。
*
*****************************************************************/
uint8_t   UnusualHandle( void );


/*****************************************************************
* Function:     EnterStop1Mode
*
* Description:  
*		此函数实现 MCU 进入低功耗 STOP1 模式，无任务处理时，MCU 进入低功耗状态，每隔 1 秒唤醒一次。
*
*****************************************************************/
void      EnterStop1Mode( void ) ;


/*****************************************************************
* Function:     LedBlink
*
* Description:  
*		此函数实现 LED 报警闪烁功能。
*
*****************************************************************/
void		 LedBlink( void );


/*****************************************************************
* Function:     Uart1RxCb
*
* Description:  
*		此函数为串口接收回调函数，实现对平台下发的数据进行判断、处理。
*
* Parameters:
*   recBuff			[in] 	串口接收缓存。
* 	size	  		[in] 	串口接收缓存长度。
*
* Warning：     传入的参数及参数指针不可为空
*
*****************************************************************/
int       Uart1RxCb( uint8_t* recBuff, uint32_t size );




