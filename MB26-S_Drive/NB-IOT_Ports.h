/** 
 *@file	     NB-IOT_Ports.h
 *@brief	   ���ݲ㹦��
 *@copyright Copyright (c) 2023,�������������������޹�˾
 *           All rights reserved.
 *@author	   Lierda-����Ӧ����
 *@date      2023-7-20
 *@version   V1.0
 */



#include <string.h>
#include <stdbool.h>
#include <stdint.h>


/* -------------------------------------------------------------------------- */

/*****************************************************************
* Function:     SysDelayMs
*
* Description:  
*		�ú���ʵ����ʱ���ܡ�
*
* Parameters:
*   delay				[in] 	��ʱʱ�䣬��λ�� ms
*
* Warning��     ����Ĳ�������Ϊ��
*
*****************************************************************/
void    SysDelayMs( uint32_t delay ) ;


/*****************************************************************
* Function:     UartTranmist
*
* Description:  
*		�û�����MCUƽ̨���ڴ�ʵ�ִ������ݷ��͹��ܡ�
*
* Parameters:
*   txBuf				[in] 	����������
*   len				  [in] 	���ݳ���
*
* Return:
* 	HAL_OK      (0x00)	�ɹ���
*		HAL_ERROR   (0x01) 	����
* 	HAL_BUSY    (0x02)	æµ��
*		HAL_TIMEOUT (0x03) 	��ʱ��
*
* Warning��     ����Ĳ���ָ�벻��Ϊ��
*
*****************************************************************/
uint8_t   UartTranmist( uint8_t* txBuf, uint16_t size );



