/** 
 *@file	     NB-IOT_Ports.c
 *@brief	   兼容层功能
 *@copyright Copyright (c) 2023,利尔达物联网技术有限公司
 *           All rights reserved.
 *@author	   Lierda-技术应用组
 *@date      2023-7-20
 *@version   V1.0
 */
 
#include "NB-IOT_Ports.h"
#include "usart.h"
#include "mcuInit.h"



/**    UART 发送函数
 @brief       用户按照MCU平台，在此实现串口数据发送功能
 @param[in]   txBuf:          发送数据指
 @param[in]   len:            数据长度
 @return      函数执行结果 
              - 成功：        HAL_OK      (0x00)
              - 错误：        HAL_ERROR   (0x01)
              - 忙碌:         HAL_BUSY    (0x02)
              - 超时：        HAL_TIMEOUT (0x03)
 @warning     传入的参数指针不可为空
*/
uint8_t UartTranmist( uint8_t* txBuf, uint16_t size )
{
		uint8_t result = 0;
	
		result = HAL_UART_Transmit( &huart1, txBuf, size, 1000 );
		
		return result;
}



/**    延时函数
 @brief       该函数用于实现系统延时功能
*@param 	    delay:          延时的时间，单位毫秒
*/
void SysDelayMs( uint32_t delay ) 
{
	uint32_t   tickstart = 0U;
	
	tickstart = GetSystemTime( );
	while( ( GetSystemTime( ) - tickstart ) < delay );
}





















