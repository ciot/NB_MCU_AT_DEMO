/** 
 *  Copyright (c) 2021,利尔达物联网技术有限公司
 *  All rights reserved.
 
 *  @brief    日志及信息输出
 *  @file     log.h
 *  @author   Gucj
 *  @version  V1.0
 *  @date     2021年2月23日
 */
 

#include <stdio.h>
#include "main.h"


#define PRINTF_ON_OFF    					1
#define AT_DEBUG_ON_OFF    				1

#if PRINTF_ON_OFF 
#define Log_Printf(str,...) printf(str,##__VA_ARGS__)
#else  
#define Log_Printf(str,...) 
#endif


#if AT_DEBUG_ON_OFF 
#define AT_Printf(USARTX,str,...) AT_Debug(USARTX,str,##__VA_ARGS__)
#else  
#define AT_Printf(USARTX,str,...) 
#endif


int      fputc(int ch,FILE *f);
void     AT_Debug(UART_HandleTypeDef* USARTX,const char *__format, ...);


