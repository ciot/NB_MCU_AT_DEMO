/*!
 *@file	     MB26-S_Drive.h
 *@brief	   MB26-S驱动层功能
 *@copyright Copyright (c) 2023,利尔达物联网技术有限公司
 *           All rights reserved.
 *@author	   Lierda-技术应用组
 *@date      2023-7-20
 *@version   V1.0
 */
 
 
/*-------------------------根据实际情况添加必要的头文件-------------------------*/
#include <stdbool.h>
#include <stdint.h>
#include <stddef.h>

/*=====================================END======================================*/

typedef enum
{   
	  AT_SUCCESS       = 1,		       /*!< AT发送成功 */
		AT_TIMEOUT       = 2,		       /*!< AT发送超时 */
		AT_ERROR         = 3,		       /*!< AT发送失败 */
		DEVICE_ON_NET    = 4,          /*!< 设备未在网 */
		DEVICE_ONLINE    = 5,          /*!< 设备在网 */
		HANDLE_FAILED    = 6,          /*!< 异常处理失败 */
		HANDLE_SUCCEED   = 7,          /*!< 异常处理成功 */
		INIT_FAILED      = 8,          /*!< 模组初始化失败 */
		INIT_SUCCEED     = 0           /*!< 模组初始化成功 */
} AtType_t;

/*------------------------------提供给用户的接口函数----------------------------*/

/*****************************************************************
* Function:     UartSendAt
*
* Description:  串口发送AT 
* 
* Parameters:
*   data				[in] 	发送的AT指令内容。
* 	size	  		[in] 	发送的AT指令长度。 
*   timeOut     [in]  接收超时时间。
* 
* Return:
* 	AT_SUCCESS			  AT发送成功。
*		AT_TIMEOUT 	      AT发送超时。
*
*****************************************************************/
uint8_t     UartSendAt( uint8_t* data, uint16_t size, uint32_t timeOut );


/*****************************************************************
* Function:     RecTimeout
*
* Description:  接收超时时间内，判断是否收到下行数据。 
* 
* Parameters: 
*   time        [in] 接收超时时间，单位毫秒。
* 
* Return:
* 	true			       成功收到数据。
*		false 	         接收超时。
*
*****************************************************************/
bool        RecTimeout( uint32_t time );


/*****************************************************************
* Function:     SendUserData
*
* Description:  
*		用户数据发送，用于向平台上报信息。
* 
* Parameters: 
*   data        [in] 待发送数据，用户可自定义，最大字节数：512。
* 
* Return:
* 	AT_SUCCESS	     数据发送成功。
*		AT_ERROR 	       数据发送失败。
*
*****************************************************************/
uint8_t     SendUserData( char* data );


/*****************************************************************
* Function:     AtChecke
*
* Description:  
*		向模组发送AT指令，检测模组是否正常上电。
* 
* Return:
* 	AT_SUCCESS	     模组已上电。
*		AT_ERROR 	       模组未上电。
*
*****************************************************************/
uint8_t     AtChecke( void );


/*****************************************************************
* Function:     GetNetStatus
*
* Description:  
*		向模组发送AT指令，查询模组网络注册状态（PS域附着状态），查询结果为1或5，表示设备已成功注册到网络。
* 
* Return:
* 	DEVICE_ONLINE	   模组已上电。
*		DEVICE_ON_NET 	 模组未上电。
*
*****************************************************************/
uint8_t     GetNetStatus( void );


/*****************************************************************
* Function:     SendCancelReq
*
* Description:  
*		向模组发送AT指令，注销平台，注册平台失败时，需要发送该指令注销平台后，再次进行注册。
* 
* Return:
* 	AT_SUCCESS	     注销成功。
*		AT_ERROR 	       注销失败。
*
*****************************************************************/
uint8_t     SendCancelReq( void );


/*****************************************************************
* Function:     SendRegReq
*
* Description:  
*		向模组发送AT指令，注册平台，注册平台失败时，需要注销平台后，再次进行注册。
* 
* Return:
* 	AT_SUCCESS	     注册成功。
*		AT_ERROR 	       注册失败。
*
*****************************************************************/
uint8_t 		SendRegReq( void );


/*****************************************************************
* Function:     ReleasRrc
*
* Description:  
*		向模组发送AT指令，该指令用于终端请求 NB-IOT 网络快速释放当前的 RRC 连接。
* 
* Return:
* 	AT_SUCCESS	     RRC释放成功。
*		AT_ERROR 	       RRC释放失败。
*
*****************************************************************/
uint8_t     ReleasRrc( void );


/*****************************************************************
* Function:     ModuleSoftReset
*
* Description:  
*		向模组发送AT指令，该指令会重启芯片。
* 
* Return:
* 	AT_SUCCESS	     复位成功。
*		AT_ERROR 	       复位成功。
*
*****************************************************************/
uint8_t     ModuleSoftReset( void );


/*****************************************************************
* Function:     SetConnePara
*
* Description:  
*		向模组发送AT指令，该指令用于设置接入物联网开放平台参数。
* 
* Parameters: 
*   severUrl    [in] 平台连接参数：域名、端口号、保活时间。
*                    不同平台域名不同，AEP平台：221.229.214.202
*                                   NB-IOT平台：180.101.147.115
* Return:
* 	AT_SUCCESS	     连接参数配置成功。
*		AT_ERROR 	       连接参数配置失败。
*
*****************************************************************/
uint8_t     SetConnePara( char *severUrl );


/*****************************************************************
* Function:     SetSleepMode
*
* Description:  
*		向模组发送AT指令，该命令可以设置 PMU 进入不同的睡眠模式。
* 
* Parameters: 
*   enable      [in] 开启/禁止 PMU（ 默认值是 1 ）：
*                    0 禁用PMU，<mode>将被忽略，系统将最低进入 IDLE 模式。
*                    1 启用PMU，可配置模组不同睡眠等级。
*   mode        [in] 睡眠深度级别（ 默认值是 1 ）：
*                    0 Active 态
*                    1 Idle 态
*                    2 Sleep1 态
*                    3 Sleep2 态
*                    4 Hibernate 态
* Return:
* 	AT_SUCCESS	     休眠模式配置成功。
*		AT_ERROR 	       休眠模式配置失败。
*
* Warning：     传入的参数指针不可为空
*
*****************************************************************/
uint8_t     SetSleepMode( int enable, int mode );


/*****************************************************************
* Function:     SetModulePsm
*
* Description:  
*		向模组发送AT指令，该指令用于设置接入物联网开放平台参数。
* 
* Parameters: 
*   mode        [in] 禁用或启用 PSM，0表示禁用PSM、1表示启用PSM，2表示禁用 PSM，参数恢复默认值。
*   ctiveTime   [in] 分配给 UE 的激活时间，一个字节用 8 位二进制格式表示，(T3324)(例如"00100100"等于 4 分钟)。
*
* Return:
* 	AT_SUCCESS	     PSM配置成功。
*		AT_ERROR 	       PSM配置失败。
*
* Warning：     启用 PSM 时，传入的参数指针不可为空
*
*****************************************************************/
uint8_t     SetModulePsm( uint8_t mode, char* ctiveTime );


/*****************************************************************
* Function:     GetSimStatus
*
* Description:  
*		向模组发送AT指令，该指令用于查询SIM卡状态是否正常。
*
* Return:
* 	AT_SUCCESS	     SIM卡状态正常。
*		AT_ERROR 	       SIM卡状态异常。
*
*****************************************************************/
uint8_t     GetSimStatus( void );


/*****************************************************************
* Function:     GetDeviceImei
*
* Description:  
*		向模组发送AT指令，发送该指令返回 IMEI 号，十进制格式的字符串类型表示 IMEI。
*
* Return:
* 	AT_SUCCESS	     获取IMEI成功。
*		AT_ERROR 	       获取IMEI失败。
*
*****************************************************************/
uint8_t     GetDeviceImei( void );


/*****************************************************************
* Function:     CloseModuleEdrx
*
* Description:  
*		向模组发送AT指令，该命令用于设置 UE 禁用 eDRX。
*
* Return:
* 	AT_SUCCESS	     eDRX关闭成功。
*		AT_ERROR 	       eDRX关闭失败。
*
*****************************************************************/
uint8_t     CloseModuleEdrx( void );


/*****************************************************************
* Function:     FuncRadio
*
* Description:  
*		向模组发送AT指令，该命令用于选择 MT 的功能等级。
*
* Parameters: 
*   mode        [in] 功能等级，1为“全功能”功耗最高，0为“最小功能”功耗最低。
*
* Return:
* 	AT_SUCCESS	     射频关闭成功。
*		AT_ERROR 	       射频关闭失败。
*
*****************************************************************/
uint8_t     FuncRadio( uint8_t mode );


/*****************************************************************
* Function:     ClearFreq
*
* Description:  
*		向模组发送AT指令，该命令用于清除保存的优先频点。
*
* Return:
* 	AT_SUCCESS	     频点清除成功。
*		AT_ERROR 	       频点清除失败。
*
*****************************************************************/
uint8_t     ClearFreq( void );


/*****************************************************************
* Function:     RegNetwork
*
* Description:  
*		向模组发送AT指令，该命令用于 MT 附着 PS 域（若为兼容海思版本，异常处理流程中加入此步骤）。
*
* Return:
* 	AT_SUCCESS	     驻网成功。
*		AT_ERROR 	       驻网失败。
*
*****************************************************************/
uint8_t     RegNetwork( void );



/*=====================================END======================================*/

