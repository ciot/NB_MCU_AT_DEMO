/** 
 *  Copyright (c) 2021,利尔达物联网技术有限公司
 *  All rights reserved.
 
 *  @brief    日志及信息输出
 *  @file     log.c
 *  @version  V1.0
 *  @date     2021年2月23日
 */

#include "log.h"
#include "usart.h"
#include <stdarg.h>
#include <string.h>

FILE __stdout;


/* 加入以下代码,支持Log_Printf函数,而不需要选择use MicroLIB */
#if defined(__CC_ARM) 
#pragma import(__use_no_semihosting)

/* 标准库需要的支持函数 */
struct __FILE
{
	int handle;
};

#elif (defined(__ARMCC_VERSION) && (__ARMCC_VERSION >= 6100100))
__ASM (".global __use_no_semihosting");
#endif



int fputc(int ch,FILE *f)
{
	while (__HAL_UART_GET_FLAG(&huart2, UART_FLAG_TC) == RESET)
	{ }
	HAL_UART_Transmit(&huart2, (uint8_t *)&ch, 1, 1000);
  return ch;
}


/* 定义_sys_exit()以避免使用半主机模式 */
void _sys_exit(int x)
{
	x=x;
}

/* 自定义串口打印函数 */
void AT_Debug( UART_HandleTypeDef* USARTX,const char *__format, ... )
{
		va_list   ap;
		va_start( ap, __format );
		uint8_t  sendBuff[ UART_MAXLEN_RX ];
	
		/* 清空发送缓冲区 */
		memset( (void*)sendBuff, (int)0, (unsigned int)UART_MAXLEN_RX );
		
		/* 填充发送缓冲区 */
		vsnprintf( (char*)sendBuff, UART_MAXLEN_RX, (const char *)__format, ap );
		va_end( ap );
		int len = strlen( (const char*)sendBuff );
		
		/* 往串口发送数据 */
		for ( int i = 0; i < len; i++ )
		{
				while ( __HAL_UART_GET_FLAG(USARTX, UART_FLAG_TC) == RESET )
				{ }
				HAL_UART_Transmit( USARTX, (uint8_t *)&sendBuff[i], 1, 1000 );
		}
}





